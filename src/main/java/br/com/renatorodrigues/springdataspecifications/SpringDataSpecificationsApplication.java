package br.com.renatorodrigues.springdataspecifications;

import br.com.renatorodrigues.springdataspecifications.repository.EmployeeRepository;
import br.com.renatorodrigues.springdataspecifications.service.EmployeeService;
import br.com.renatorodrigues.springdataspecifications.specification.EmployeeSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories()
public class SpringDataSpecificationsApplication implements CommandLineRunner{

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringDataSpecificationsApplication.class, args);
	}
	
	@Override
	public void run(String... strings) throws Exception {
		
		System.out.println(employeeRepository.findAll(EmployeeSpecification.senior()));
	}
}
