package br.com.renatorodrigues.springdataspecifications.specification;

import br.com.renatorodrigues.springdataspecifications.entity.Employee;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.sql.Date;

/**
 * Created by Ballem on 25/02/2016.
 */
public class EmployeeSpecification {

    public static Specification<Employee> senior() {
        return new Specification<Employee>() {

        	
			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				Expression<Date> date1 = root.get("hireDate");
				Expression<Date> date2 = cb.currentDate();
				
				return cb.greaterThan(cb.function("datediff", Integer.class, date1, date2),10);
			}
   
		};
    }

    public static Specification<Employee> sexo(String sexo) {
    	
    	return new Specification<Employee>() {

			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				return cb.like(root.get("sexo"), sexo);
			}
   
		};
    }

    public static Specification<Employee> firstName(String firstName) {
    	return new Specification<Employee>() {

			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				return cb.like(root.get("firstName"), firstName);
			}
   
		};
    }

    public static Specification<Employee> lastName(String lastName) {
    	return new Specification<Employee>() {

			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				return cb.like(root.get("lastName"), lastName);
			}
   
		};
    }
    
    public static Specification<Employee> idade(Integer idade) {
    	return new Specification<Employee>() {

			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				
				query.orderBy(
						cb.asc(root.get("nome")), cb.asc(root.get("sobrenome"))
				);
				
				return cb.equal(root.get("idade"), idade);
			}
        	
		};
    }
}
