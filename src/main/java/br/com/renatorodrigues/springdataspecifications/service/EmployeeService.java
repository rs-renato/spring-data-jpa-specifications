package br.com.renatorodrigues.springdataspecifications.service;

import br.com.renatorodrigues.springdataspecifications.entity.Employee;
import br.com.renatorodrigues.springdataspecifications.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by renatorodrigues on 13/08/17.
 */
@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	
	public List<Employee> findAll(Specification<Employee> specification){
		return employeeRepository.findAll(specification);
	}
}
